#!/bin/bash
# 
# Copyright (c) 2014 Simon Desvergez contact@overbox.net under licence ISC
# https://github.com/sdesvergez/vm-tools-installer
#
clear
echo "**** VMware tools installer for GNU/Linux ****"

# check if it's a virtual machine - the package pciutils is required.
lspci > /dev/null 2>&1 || { echo "ERROR : Please install pciutils package from your favorite repository !" >&2; }
lspci | grep -i "vmware" > /dev/null
if [ $? -eq 0 ]; then
        echo "OK : VMware guest detected..."
else
        echo "ERROR : It seems we're not on a VMware guest... Bye !"
        exit ;
fi

# check previous installation
cd /tmp
[ -d /tmp/vmware-tools-distrib ] && rm -rf /tmp/vmware-tools-distrib/*
if [ -d /etc/vmware-tools ];
then
	echo "WARNING : Target directory already exists..."
	vmware-toolbox-cmd -v >/dev/null 2>&1 || { echo "OK : ...but VMware tools have to be installed." >&2; }
	echo -n "Press [Enter] key to continue or Ctrl+C to abort..."
	read var_continue
fi

# check if Install VMware Tools has been initiated from Host
mount /dev/cdrom /mnt 2>&1 || { echo "ERROR : Select Install VMware tools from VMware Host (ESX, ESXi, your PC or Mac...) and try again !" >&2 && exit ;}

DIST=$(cat /proc/version)
while true; do
  case $DIST in
    *debian*    )	echo "OK : Debian detected..." ; apt-get install -y build-essential linux-headers-$(uname -r) psmisc libglib2.0 ; break ;;
    *ubuntu*   	)	echo "OK : Ubuntu detected..." ; aptitude install -y build-essential linux-headers-$(uname -r) psmisc libglib2.0 ; break ;;
    *redhat*    )	echo "OK : RedHat detected..." ; yum install -y make gcc kernel-devel kernel-headers glibc-headers perl ; break ;;
    *centos*    ) 	echo "OK : CentOS detected..." ; yum install -y make gcc kernel-devel kernel-headers glibc-headers perl ; break ;;
    *           )	echo "WARNING : Unknown Operating System. Change and use this script at your own risks !" ; exit  ;;
  esac
done

ln -s /usr/src/linux-headers-$(uname -r) /usr/src/linux
export CC=$(which gcc)

tar zxvf /mnt/VMwareTools-*.tar.gz > /dev/null
umount /mnt
cd /tmp/vmware-tools-distrib/
./vmware-install.pl -d
exit
