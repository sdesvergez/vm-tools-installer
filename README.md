How to install
==============

Tested operating systems : Debian 6/7, Ubuntu 12/14, Centos 4/5/6, RHEL 5/6/7

From servers running tested operating systems simply run as root :
```
cd /tmp && wget https://goo.gl/E0bGUW -O vm-tools-installer.sh && sh ./vm-tools-installer.sh
```
Download source from https://bitbucket.org/sdesvergez/vm-tools-installer/downloads


Support
=======
See details at https://bitbucket.org/sdesvergez/vm-tools-installer/wiki/

Feel free to report issues at https://bitbucket.org/sdesvergez/vm-tools-installer/issues
